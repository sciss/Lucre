#!/bin/bash
sbt -J-Xmx2G "; + adjunctJS/test ; + adjunctJVM/test ; + baseJS/test ; + baseJVM/test ; + confluentJS/test ; + confluentJVM/test ; + coreJS/test ; + coreJVM/test ; + dataJS/test ; + dataJVM/test ; + exprJS/test ; + exprJVM/test ; + geomJS/test ; + geomJVM/test ; + lucre-bdb/test"

echo "--- testJS ---"
sbt -J-Xmx4G "; + testsJS/test"
echo "--- testJVM, Scala 2.12 ---"
sbt -J-Xmx4G "; ++2.12.16 testsJVM/test"
echo "--- testJVM, Scala 2.13 ---"
sbt -J-Xmx4G "; ++2.13.8 testsJVM/test"
echo "--- testJVM, Scala 3 ---"
sbt -J-Xmx4G "; ++3.1.3 testsJVM/test"
