package de.sciss.lucre.expr
package examples

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.{InMemory, Workspace}

object ExSeqBug {
  def main(args: Array[String]): Unit = run()

  type S = InMemory
  type T = InMemory.Txn

  def run(): Unit = {
    val g = Graph {
      import graph._

      val NumRuns = 2
      val lb      = LoadBang()
      val vrPos   = Var(Seq.empty[Int])
      val run     = Var(0)
      val cmAcc   = Var(Seq.empty[Int])

      val TEST: Ex[Seq[Seq[Int]]] =
        Seq(
          Seq(1000, 2000, 3000),
          Seq(1000, 2000, 3000, 44000, 45000),
        )

      val rPeaksDone = Trig()

      val actRun = Act(
        vrPos.set(TEST.applyOption(run).getOrElse(Nil)),
        rPeaksDone
      )

      val cmAccAll = Var(Seq.empty[Int])

      val cmGroups: Ex[Seq[Seq[Int]]] = cmAccAll /*cmAcc*/.map(x => Seq(x): Ex[Seq[Int]])
      val cmFound : Ex[Seq[Seq[Int]]] = cmGroups.filter(_.size >= 2)
      val cmMean  : Ex[Seq[Int]]      = cmFound.map(xs => xs.sum)

      rPeaksDone --> Act(
        cmAcc.set(cmAcc ++ vrPos),
        run.inc,
        If (run < NumRuns) Then actRun Else Act(
          cmAccAll.set(cmAcc),  // work-around for issue #68
          PrintLn(cmMean.size.toStr)
        )
      )

      lb --> actRun
    }

    implicit val system: S = InMemory()
    implicit val undo: UndoManager[T] = UndoManager()

    import Workspace.Implicits._

//    Log.event.level = Level.All

    for (i <- 0 until 10) {
      println(s"attempt ${i + 1}")
      system.step { implicit tx =>
        implicit val ctx: Context[T] = Context(None)
        g.expand[T].initControl()
      }
    }
  }
}
