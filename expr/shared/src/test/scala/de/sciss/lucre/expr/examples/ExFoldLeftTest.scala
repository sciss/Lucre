//package de.sciss.lucre.expr.examples
//
//import de.sciss.lucre.edit.UndoManager
//import de.sciss.lucre.expr.ExImport._
//import de.sciss.lucre.expr.graph._
//import de.sciss.lucre.expr.{Context, Graph}
//import de.sciss.lucre.{InMemory, IntObj, IntVector, Workspace}
//
///*
//  expected output:
//
//  10
//  13
//  3
//  5
//
// */
//object ExFoldLeftTest extends App {
//  type S = InMemory
//  type T = InMemory.Txn
//
//  val g = Graph {
//    val zAttr   = "z"   .attr[Int](0)
//    val sqAttr  = "seq" .attr[Seq[Int]](Nil)
//    val m = sqAttr.foldLeft(zAttr) { case (aggr, x) =>
//      aggr + x
//    }
//    m.changed --> PrintLn(m.toStr)
//  }
//
//  implicit val system: S = InMemory()
//  implicit val undo: UndoManager[T] = UndoManager()
//
//  import Workspace.Implicits._
//
//  system.step { implicit tx =>
//    val self  = IntObj.newConst(0): IntObj[T]
//    val selfH = tx.newHandle(self)
//    implicit val ctx: Context[T] = Context(Some(selfH))
//    g.expand[T].initControl()
//    val a = self.attr
//    a.put("z"   , IntObj.newConst(10))
//    a.put("seq" , IntVector.newConst(Vector(1, 2)))
//    a.put("z"   , IntObj.newConst(0))
//    a.put("seq" , IntVector.newConst(Vector(2, 3)))
//  }
//}
