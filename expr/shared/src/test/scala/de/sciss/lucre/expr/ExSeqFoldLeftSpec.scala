package de.sciss.lucre.expr

import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.{InMemory, IntObj, IntVector, StringObj, Workspace}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

/*

    testOnly de.sciss.lucre.expr.ExSeqFoldLeftSpec

 */
class ExSeqFoldLeftSpec extends AnyFlatSpec with Matchers with CaptureConsoleOutput {
  type S = InMemory
  type T = InMemory.Txn

  "Ex[Seq[A]].foldLeft" should "work as expected" in {
    val g = Graph {
      import ExImport._
      import graph._

      val in: Ex[Seq[Int]] = "in"   .attr(Seq.empty[Int])
      val z : Ex[Int]      = "zero" .attr(0)
      val out = in.foldLeft(z) { (aggr, x) => aggr + x }
      val b   = LoadBang()
      val c   = out /*in*/.changed
      val t   = b | c
      t --> PrintLn(Const("out = ") ++ out.toStr)
    }

    implicit val system: S = InMemory()
    implicit val undo: UndoManager[T] = UndoManager()

    import Workspace.Implicits._

    val res: String = captureConsole {
      system.step { implicit tx =>
        val self: StringObj[T] = StringObj.newConst("self")
        val selfH = tx.newHandle(self)
        implicit val ctx: Context[T] = Context(Some(selfH))
        g.expand[T].initControl()
        val a = self.attr
        a.put("in"    , IntVector .newConst(Vector(1, 2)))
        a.put("in"    , IntVector .newConst(Vector(3, 4)))
        a.put("zero"  , IntObj    .newConst(100))
      }
    }
    val exp: String =
      """out = 0
        |out = 3
        |out = 7
        |out = 107
        |""".stripMargin

    assert (res == exp)
  }
}
