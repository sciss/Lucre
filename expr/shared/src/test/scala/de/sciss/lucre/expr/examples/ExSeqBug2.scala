package de.sciss.lucre.expr
package examples

import de.sciss.log.Level
import de.sciss.lucre.edit.UndoManager
import de.sciss.lucre.{InMemory, Log, Workspace}
import scala.Predef.{any2stringadd => _, _}

object ExSeqBug2 {
  def main(args: Array[String]): Unit = run()

  type S = InMemory
  type T = InMemory.Txn

  def run(): Unit = {
    val g = Graph {
      import graph._
      import ExImport._

      val weightSeq: Ex[Seq[Seq[Double]]] = Seq(
        Seq(1.0),
        Seq(0.4, 0.7, 0.9, 1.0),
      )

      val genNumParam = Var(0)
      val weightRnd   = Var(0.0)
      val patchIdx    = weightSeq(genNumParam).indexWhere(_ < weightRnd) + 1

      patchIdx.changed --> PrintLn("patchIdx " ++ patchIdx.toStr)

      LoadBang() --> Act(
        genNumParam.set(1),
        weightRnd.set(0.99),
      )
    }

    implicit val system: S = InMemory()
    implicit val undo: UndoManager[T] = UndoManager()

    import Workspace.Implicits._

    Log.event.level = Level.All

    system.step { implicit tx =>
      implicit val ctx: Context[T] = Context(None)
      g.expand[T].initControl()
    }
  }
}
