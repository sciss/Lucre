/*
 *  ExSeq.scala
 *  (Lucre 4)
 *
 *  Copyright (c) 2009-2024 Hanns Holger Rutz. All rights reserved.
 *
 *  This software is published under the GNU Affero General Public License v3+
 *
 *
 *  For further information, please contact Hanns Holger Rutz at
 *  contact@sciss.de
 */

package de.sciss.lucre.expr

import de.sciss.lucre.Txn.peer
import de.sciss.lucre.expr.ExElem.{ProductReader, RefMapIn}
import de.sciss.lucre.expr.graph.impl.{ExpandedMapSeqIn, MappedIExpr}
import de.sciss.lucre.expr.graph.{Ex, It, Obj}
import de.sciss.lucre.impl.IChangeEventImpl
import de.sciss.lucre.{Adjunct, Caching, Disposable, IChangeEvent, IExpr, IPull, IPush, ITargets, ProductWithAdjuncts, Txn, expr}

import scala.concurrent.stm.Ref

object ExSeq extends ProductReader[ExSeq[_]] {
  private def assertSameSize[A, B](tuples: Seq[A], inV: Seq[B]): Unit = {
//    assert (tuples.size == inV.size, s"tuples.size (${tuples.size}) != inV.size (${inV.size})")
    if (tuples.size != inV.size) {
      throw new AssertionError(s"tuples.size (${tuples.size}) != inV.size (${inV.size})")
    }
  }

  private final class Expanded[T <: Txn[T], A](elems: Seq[IExpr[T, A]])(implicit protected val targets: ITargets[T])
    extends IExpr[T, Seq[A]] with IChangeEventImpl[T, Seq[A]] {

    def init()(implicit tx: T): this.type = {
      elems.foreach { in =>
        in.changed ---> changed
      }
      this
    }

    def value(implicit tx: T): Seq[A] = elems.map(_.value)

    def dispose()(implicit tx: T): Unit = {
      elems.foreach { in =>
        in.changed -/-> changed
      }
    }

    def changed: IChangeEvent[T, Seq[A]] = this

    private[lucre] def pullChange(pull: IPull[T])(implicit tx: T, phase: IPull.Phase): Seq[A] = {
      val b = Seq.newBuilder[A]
      b.sizeHint(elems)
      elems.foreach { in =>
        val v = pull.expr(in)
        b += v
      }
      b.result()
    }
  }

  private final class CountExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                    fun: Ex[Boolean], tx0: T)
                                                   (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Int](in, it, fun, tx0) {

    override def toString: String = s"$in.count($fun)"

    protected def emptyOut: Int = 0

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Int = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      var res       = 0
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (funV) res += 1
      }
      res
    }
  }

  object Count extends ProductReader[Count[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Count[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new Count(_in, _it, _p)
    }
  }
  final case class Count[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Int] {

    type Repr[T <: Txn[T]] = IExpr[T, Int]

    override def productPrefix: String = s"ExSeq$$Count" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new CountExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class DropWhileExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                        fun: Ex[Boolean], tx0: T)
                                                       (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Seq[A]](in, it, fun, tx0) {

    override def toString: String = s"$in.dropWhile($fun)"

    protected def emptyOut: Seq[A] = Nil

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Seq[A] = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (!funV) {
          val b = Seq.newBuilder[A]
          b += vn
          while (iterator.hasNext) {
            val (vn, _) = iterator.next()
            b += vn
          }
          return b.result()
        }
      }
      Nil
    }
  }

  object DropWhile extends ProductReader[DropWhile[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): DropWhile[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new DropWhile(_in, _it, _p)
    }
  }
  final case class DropWhile[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Seq[A]] {

    type Repr[T <: Txn[T]] = IExpr[T, Seq[A]]

    override def productPrefix: String = s"ExSeq$$DropWhile" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new DropWhileExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class ExistsExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                     fun: Ex[Boolean], tx0: T)
                                                    (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Boolean](in, it, fun, tx0) {

    override def toString: String = s"$in.exists($fun)"

    protected def emptyOut: Boolean = false

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Boolean = {
      assertSameSize (tuples, inV)
      val iterator = inV.iterator zip tuples.iterator
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (funV) return true
      }
      false
    }
  }

  object Exists extends ProductReader[Exists[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Exists[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new Exists(_in, _it, _p)
    }
  }
  final case class Exists[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Boolean] {

    type Repr[T <: Txn[T]] = IExpr[T, Boolean]

    override def productPrefix: String = s"ExSeq$$Exists" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new ExistsExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class FilterExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                     fun: Ex[Boolean], tx0: T)
                                                    (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Seq[A]](in, it, fun, tx0) {

    override def toString: String = s"$in.filter($fun)"

    protected def emptyOut: Seq[A] = Nil

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Seq[A] = {
      assertSameSize (tuples, inV)    // XXX TODO this fails sometimes
      val iterator  = inV.iterator zip tuples.iterator
      val b         = Seq.newBuilder[A]
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (funV) b += vn
      }
      b.result()
    }
  }

  object Filter extends ProductReader[Filter[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Filter[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new Filter(_in, _it, _p)
    }
  }
  final case class Filter[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Seq[A]] {

    type Repr[T <: Txn[T]] = IExpr[T, Seq[A]]

    override def productPrefix: String = s"ExSeq$$Filter" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new FilterExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class FilterNotExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                        fun: Ex[Boolean], tx0: T)
                                                       (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Seq[A]](in, it, fun, tx0) {

    override def toString: String = s"$in.filterNot($fun)"

    protected def emptyOut: Seq[A] = Nil

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Seq[A] = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      val b         = Seq.newBuilder[A]
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (!funV) b += vn
      }
      b.result()
    }
  }

  object FilterNot extends ProductReader[FilterNot[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FilterNot[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new FilterNot(_in, _it, _p)
    }
  }
  final case class FilterNot[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Seq[A]] {

    type Repr[T <: Txn[T]] = IExpr[T, Seq[A]]

    override def productPrefix: String = s"ExSeq$$FilterNot" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new FilterNotExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class FindExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                   fun: Ex[Boolean], tx0: T)
                                                  (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Option[A]](in, it, fun, tx0) {

    override def toString: String = s"$in.find($fun)"

    protected def emptyOut: Option[A] = None

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Option[A] = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (funV) return Some(vn)
      }
      None
    }
  }

  object Find extends ProductReader[Find[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Find[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new Find(_in, _it, _p)
    }
  }
  final case class Find[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Option[A]] {

    type Repr[T <: Txn[T]] = IExpr[T, Option[A]]

    override def productPrefix: String = s"ExSeq$$Find" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new FindExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class FindLastExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                       fun: Ex[Boolean], tx0: T)
                                                      (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Option[A]](in, it, fun, tx0) {

    override def toString: String = s"$in.findLast($fun)"

    protected def emptyOut: Option[A] = None

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Option[A] = {
      assertSameSize (tuples, inV)
      val iterator  = inV.reverseIterator zip tuples.reverseIterator
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (funV) return Some(vn)
      }
      None
    }
  }

  object FindLast extends ProductReader[FindLast[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FindLast[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new FindLast(_in, _it, _p)
    }
  }
  final case class FindLast[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Option[A]] {

    type Repr[T <: Txn[T]] = IExpr[T, Option[A]]

    override def productPrefix: String = s"ExSeq$$FindLast" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new FindLastExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  // XXX TODO: DRY with ExpandedMapSeqOrOption
  private final class FoldLeftExpanded[T <: Txn[T], A, B](in: IExpr[T, Seq[A]],
                                                          z: IExpr[T, B],
                                                          it: It.Expanded[T, (B, A)],
                                                          op: Ex[B], tx0: T)
                                                  (implicit protected val targets: ITargets[T], ctx: Context[T])
    extends IExpr[T, B] with IChangeEventImpl[T, B] with Caching {

    private type In[~]  = Seq[~]
    private type Out    = B
//    private type P      = (B, A)

    protected type Tuples = In[(IExpr[T, B], Disposable[T])]

    // ---- abstract: in ----

    protected def isEmpty[A1](in: In[A1]): Boolean = in.isEmpty

    protected def foreach[A1](in: In[A1])(body: A1 => Unit): Unit = in.foreach(body)

    protected def emptyIn[A1]: In[A1] = Seq.empty[A1]

    protected def emptyOut(zV: B): Out = zV

    protected def map[A1, B1](in: In[A1])(body: A1 => B1): In[B1] = in.map(body)

//    protected def buildResult(inV: In[A], tuples: Tuples)(elem: IExpr[T, P] => P)(implicit tx: T): Out

    // ---- impl ----

    private val ref = Ref.make[Tuples]()

    private def init()(implicit tx: T): Unit = {
      in .changed.--->(this)
      z  .changed.--->(this)
      val inV   = in.value
      val zV    = z .value
      val refV  = mkRef(inV, zV)
      ref()     = refV
    }

    init()(tx0)

    private def disposeRef(refV: Tuples)(implicit tx: T): Unit =
      foreach(refV) { case (f, d) =>
        f.changed -/-> this
        d.dispose()
      }

    private[lucre] def pullChange(pull: IPull[T])(implicit tx: T, phase: IPull.Phase): Out = {
      val inV = pull.expr(in)
      val zV  = pull.expr(z )
      // XXX TODO : correct?
      if ((pull.contains(in.changed) || pull.contains(z.changed)) && phase.isNow) {
        val refV = mkRef(inV, zV)
        disposeRef(ref.swap(refV))
      }
      if (isEmpty(inV)) emptyOut(zV)
      else {
        buildResult(inV, zV, ref())(pull.expr(_))
      }
    }

    private def mkRef(inV: In[A], zV: B)(implicit tx: T): Tuples = {
      var aggr = zV
      map[A, (IExpr[T, B], Disposable[T])](inV) { v =>
        it.setValue((aggr, v))
        val (f, d) = ctx.nested(it) {
          val _f = op.expand[T]
          _f.changed ---> this
          aggr = _f.value
          _f
        }
        (f, d)
      }
    }

    override def value(implicit tx: T): Out =
      IPush.tryPull(this).fold({
        val inV = in.value
        val zV  = z .value
        if (isEmpty(inV)) emptyOut(zV)
        else {
          buildResult(inV, zV, ref())(_.value)
        }
      })(_.now)

    override def dispose()(implicit tx: T): Unit = {
      in .changed.-/->(this)
      disposeRef(ref.swap(emptyIn))
    }

    override def changed: IChangeEvent[T, Out] = this

    override def toString: String = s"$in.foldLeft($z)($op)"

    protected def buildResult(inV: Seq[A], zV: B, tuples: Tuples)(elem: IExpr[T, B] => B)
                             (implicit tx: T): B = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      var aggr      = zV
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue((aggr, vn))
        val funV = elem(f)
        aggr = funV
      }
      aggr
    }
  }

  object FoldLeft extends ProductReader[FoldLeft[_, _]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): FoldLeft[_, _] = {
      require (arity == 4 && adj == 0)
      type A = Any
      type B = Any
      val _in = in.readEx[Seq[A]]()
      val _z  = in.readEx[B]()
      val _it = in.readProductT[It[(B, A)]]()
      val _op = in.readEx[B]()
      new FoldLeft[A, B](_in, _z, _it, _op)
    }
  }
  final case class FoldLeft[A, B](in: Ex[Seq[A]], z: Ex[B], it: It[(B, A)], op: Ex[B])
    extends Ex[B] {

    type Repr[T <: Txn[T]] = IExpr[T, B]

    override def productPrefix: String = s"ExSeq$$FoldLeft" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val zEx  = z .expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new FoldLeftExpanded[T, A, B](inEx, zEx, itEx, op, tx)
    }
  }


  private final class ForallExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                     fun: Ex[Boolean], tx0: T)
                                                    (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Boolean](in, it, fun, tx0) {

    override def toString: String = s"$in.forall($fun)"

    protected def emptyOut: Boolean = true

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Boolean = {
      assertSameSize (tuples, inV)
      val iterator = inV.iterator zip tuples.iterator
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (!funV) return false
      }
      true
    }
  }

  object Forall extends ProductReader[Forall[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Forall[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new Forall(_in, _it, _p)
    }
  }
  final case class Forall[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Boolean] {

    type Repr[T <: Txn[T]] = IExpr[T, Boolean]

    override def productPrefix: String = s"ExSeq$$Forall" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new ForallExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  private final class IndexWhereExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                         fun: Ex[Boolean], tx0: T)
                                                        (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Int](in, it, fun, tx0) {

    override def toString: String = s"$in.indexWhere($fun)"

    protected def emptyOut: Int = -1

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Int = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      var res       = 0
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (funV) return res
        res += 1
      }
      -1
    }
  }

  object IndexWhere extends ProductReader[IndexWhere[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): IndexWhere[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new IndexWhere(_in, _it, _p)
    }
  }
  final case class IndexWhere[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Int] {

    type Repr[T <: Txn[T]] = IExpr[T, Int]

    override def productPrefix: String = s"ExSeq$$IndexWhere" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new IndexWhereExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  // XXX TODO --- we should use cell-views instead, because this way we won't notice
  // changes to the value representation (e.g. a `StringObj.Var` contents change)
  private final class SelectExpanded[T <: Txn[T], A](in: IExpr[T, Seq[Obj]], tx0: T)
                                                    (implicit targets: ITargets[T], bridge: Obj.Bridge[A])
    extends MappedIExpr[T, Seq[Obj], Seq[A]](in, tx0) {

    protected def mapValue(inValue: Seq[Obj])(implicit tx: T): Seq[A] =
      inValue.flatMap(_.peer[T].flatMap(bridge.tryParseObj(_)))
  }

  object Select extends ProductReader[Select[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): Select[_] = {
      require (arity == 1 && adj == 1)
      val _in = in.readEx[Seq[Obj]]()
      val _bridge: Obj.Bridge[Any] = in.readAdjunct()
      new Select[Any](_in)(_bridge)
    }
  }
  final case class Select[A](in: Ex[Seq[Obj]])(implicit bridge: Obj.Bridge[A])
    extends Ex[Seq[A]] with ProductWithAdjuncts {

    type Repr[T <: Txn[T]] = IExpr[T, Seq[A]]

    override def productPrefix: String = s"ExSeq$$Select" // serialization

    def adjuncts: List[Adjunct] = bridge :: Nil

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      import ctx.targets
      new SelectExpanded[T, A](inEx, tx)
    }
  }

  // XXX TODO --- we should use cell-views instead, because this way we won't notice
  // changes to the value representation (e.g. a `StringObj.Var` contents change)
  private final class SelectFirstExpanded[T <: Txn[T], A](in: IExpr[T, Seq[Obj]], tx0: T)
                                                         (implicit targets: ITargets[T], bridge: Obj.Bridge[A])
    extends MappedIExpr[T, Seq[Obj], Option[A]](in, tx0) {

    protected def mapValue(inValue: Seq[Obj])(implicit tx: T): Option[A] = {
      val it = inValue.iterator.flatMap(_.peer[T].flatMap(bridge.tryParseObj(_)))
      if (it.hasNext) Some(it.next()) else None
    }
  }

  object SelectFirst extends ProductReader[SelectFirst[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): SelectFirst[_] = {
      require (arity == 1 && adj == 1)
      val _in = in.readEx[Seq[Obj]]()
      val _bridge: Obj.Bridge[Any] = in.readAdjunct()
      new SelectFirst[Any](_in)(_bridge)
    }
  }
  final case class SelectFirst[A](in: Ex[Seq[Obj]])(implicit bridge: Obj.Bridge[A])
    extends Ex[Option[A]] with ProductWithAdjuncts {

    type Repr[T <: Txn[T]] = IExpr[T, Option[A]]

    override def productPrefix: String = s"ExSeq$$SelectFirst" // serialization

    def adjuncts: List[Adjunct] = bridge :: Nil

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      import ctx.targets
      new SelectFirstExpanded[T, A](inEx, tx)
    }
  }

  private final class TakeWhileExpanded[T <: Txn[T], A](in: IExpr[T, Seq[A]], it: It.Expanded[T, A],
                                                        fun: Ex[Boolean], tx0: T)
                                                       (implicit targets: ITargets[T], ctx: Context[T])
    extends ExpandedMapSeqIn[T, A, Boolean, Seq[A]](in, it, fun, tx0) {

    override def toString: String = s"$in.takeWhile($fun)"

    protected def emptyOut: Seq[A] = Nil

    protected def buildResult(inV: Seq[A], tuples: Tuples)(elem: IExpr[T, Boolean] => Boolean)
                             (implicit tx: T): Seq[A] = {
      assertSameSize (tuples, inV)
      val iterator  = inV.iterator zip tuples.iterator
      val b = Seq.newBuilder[A]
      while (iterator.hasNext) {
        val (vn, (f, _)) = iterator.next()
        it.setValue(vn)
        val funV = elem(f)
        if (!funV) {
          return b.result()
        }
        b += vn
      }
      b.result()
    }
  }

  object TakeWhile extends ProductReader[TakeWhile[_]] {
    override def read(in: RefMapIn, key: String, arity: Int, adj: Int): TakeWhile[_] = {
      require (arity == 3 && adj == 0)
      val _in = in.readEx[Seq[Any]]()
      val _it = in.readProductT[It[Any]]()
      val _p  = in.readEx[Boolean]()
      new TakeWhile(_in, _it, _p)
    }
  }
  final case class TakeWhile[A](in: Ex[Seq[A]], it: It[A], p: Ex[Boolean])
    extends Ex[Seq[A]] {

    type Repr[T <: Txn[T]] = IExpr[T, Seq[A]]

    override def productPrefix: String = s"ExSeq$$TakeWhile" // serialization

    protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
      val inEx = in.expand[T]
      val itEx = it.expand[T]
      import ctx.targets
      new TakeWhileExpanded[T, A](inEx, itEx, p, tx)
    }
  }

  override def read(in: RefMapIn, key: String, arity: Int, adj: Int): ExSeq[_] = {
    require (arity == 1 && adj == 0)
    val _elems = in.readVec(in.readEx[Any]())
    new ExSeq(_elems: _*)
  }
}
final case class ExSeq[A](elems: Ex[A]*) extends Ex[Seq[A]] {

  type Repr[T <: Txn[T]] = IExpr[T, Seq[A]]

  private def simpleString: String = {
    val xs = elems.iterator.take(5).toList
    val es = if (xs.lengthCompare(5) == 0) xs.init.mkString("", ", ", ", ...")
    else xs.mkString(", ")
    s"ExSeq($es)"
  }

  override def toString: String = simpleString

  protected def mkRepr[T <: Txn[T]](implicit ctx: Context[T], tx: T): Repr[T] = {
    import ctx.targets
    val elemsEx: Seq[IExpr[T, A]] = elems.iterator.map(_.expand[T]).toList
    new expr.ExSeq.Expanded(elemsEx).init()
  }
}
